---
title: 关于
date: 2019-06-15 11:07:01
---
## 坐标

**成都.四川.中国**

## 就职与工作

* 2017.3 ~ [成都青之软件](http://www.qzsoft.net/)
    * 脚本系统（C#）脚本编辑器、脚本执行、脚本结果处理 ... ;
    * 文件内容分析与提取 PDF、Excel、Word、txt ... ;
    * 硬件系统集成 ：串口 、TCP/IP 、Modbus Tcp ... ;
* 2015.10 ~ 2016.12 [成都亿合科技](http://www.ehecd.com/)
    * 电商软件研发
* 2013.3 ~ 2015.2 [重庆梅安森科技股份](http://www.cqmas.com/)
    * 监控软件研发 
    * 硬件系统集成

## 技术栈 & 熟练 [ ***** ]

* [****] CSharp   
* [****] .NET Framework [WinForm WebApi EF ...]
* [**] .Net Core 
---
* [****] MS SqlServer
* [**] MySql
* [**] Mongodb
* [***] Redis
* [***] RabbitMQ
---
* [****] JavaScript
* [***] TypeScript
* [****] Vue(全家桶)
* [***] Js跨平台
* [***] Html&Css&JQuery 
---
* [***] NodeJs
* [**] 杂碎

**ing**
* .Net Core
* ts

## 混迹网络

**某乎** **豆瓣** **B站** 

**博客园** **掘金** **简书**