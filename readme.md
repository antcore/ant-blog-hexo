cd ~/code/blog 

docker build -t bloghexo .

docker run -d -p 8081:80 \
-v $PWD/build:/usr/local/apache2/htdocs/ \
--name bloghexo --privileged=true --restart=always bloghexo

---

cd /home/up/

rm -rf /root/code/blog/build/*

tar zxvf blog.tar.gz -C /root/code/blog/build/

rm -rf blog.tar.gz

cd /

cd /root/code

docker ps -a

docker stop blog

docker rm blog

docker rmi hexoblog

docker build -t hexoblog . 

docker run -d -p 8081:80 --name blog --privileged=true --restart=always hexoblog

